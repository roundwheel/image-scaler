package ca.roundwheel.scaler;

import static org.junit.Assert.assertEquals;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ca.roundwheel.scaler.fileio.FileIOHandler;
import ca.roundwheel.scaler.imageio.ImageIOHandler;

/**
 * Tests {@code ImageScaler}.
 * 
 * @author Yvon Comeau
 */
public class ImageScalerTest {
	
	// The object we are testing
	private ImageScaler imageScaler;

	// Mocked dependencies
	private ImageIOHandler mockedImageIOHandler = Mockito.mock(ImageIOHandler.class);
	private FileIOHandler mockedFileIOHandler = Mockito.mock(FileIOHandler.class);
	
	@Before
	public void init() {
		imageScaler = new ImageScaler(mockedImageIOHandler, mockedFileIOHandler);
	}
	
	/**
	 * enforceImageSize tests
	 */

	@Test(expected=NullPointerException.class)
	public void testEnforceImageSizeNullPath() throws IOException {
		imageScaler.enforceImageSize(null, 0.0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEnforceImageSizeZeroSize() throws IOException {
		imageScaler.enforceImageSize("foo", 0.0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEnforceImageSizeNegativeSize() throws IOException {
		imageScaler.enforceImageSize("foo", -1.0);
	}
	
	@Test
	public void testEnforceImageSizeImageNarrowerThanRequested() throws IOException {
		
		// createImageFromFilename
		BufferedImage mockedOriginalBufferedImage = Mockito.mock(BufferedImage.class);
		Mockito.when(mockedImageIOHandler.createImageFromFilename(Mockito.anyString())).thenReturn(mockedOriginalBufferedImage);
		
		Mockito.when(mockedOriginalBufferedImage.getWidth()).thenReturn(1);
		Mockito.when(mockedOriginalBufferedImage.getHeight()).thenReturn(1);
		
		Dimension result = imageScaler.enforceImageSize("/", 2.0);
		assertEquals(1, result.getHeight(), 1.0);
		assertEquals(1, result.getWidth(), 1.0);
	}
	
	@Test
	public void testEnforceImageSizeImageWiderThanRequested() throws Exception {

		// createImageFromFilename
		BufferedImage mockedOriginalBufferedImage = Mockito.mock(BufferedImage.class);
		
		Mockito.when(mockedImageIOHandler.createImageFromFilename(Mockito.anyString())).thenReturn(mockedOriginalBufferedImage);
		
		Mockito.when(mockedOriginalBufferedImage.getWidth()).thenReturn(4);
		Mockito.when(mockedOriginalBufferedImage.getHeight()).thenReturn(1);
		
		// scaleImage
		BufferedImage mockedBufferedImageResponse = Mockito.mock(BufferedImage.class);
		Mockito.when(mockedImageIOHandler.scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble())).thenReturn(mockedBufferedImageResponse);
		Mockito.when(mockedBufferedImageResponse.getWidth()).thenReturn(3);
		Mockito.when(mockedBufferedImageResponse.getHeight()).thenReturn(2);
		
		// saveImage
		Mockito.doNothing().when(mockedImageIOHandler).saveImage(Mockito.any(BufferedImage.class), Mockito.anyString());
		
		Dimension result = imageScaler.enforceImageSize("/", 2.0);

		Mockito.verify(mockedImageIOHandler, Mockito.times(1)).createImageFromFilename("/");
		Mockito.verify(mockedOriginalBufferedImage, Mockito.times(1)).getWidth();
		Mockito.verify(mockedOriginalBufferedImage, Mockito.times(1)).getHeight();
		Mockito.verify(mockedImageIOHandler, Mockito.times(1)).scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble());
		Mockito.verify(mockedImageIOHandler, Mockito.times(1)).saveImage(mockedBufferedImageResponse, "/");
		Mockito.verify(mockedBufferedImageResponse, Mockito.times(1)).getWidth();
		Mockito.verify(mockedBufferedImageResponse, Mockito.times(2)).getHeight();
		
		assertEquals(3, result.getWidth(), 1.0);
		assertEquals(2, result.getHeight(), 1.0);
	}
	
	@Test
	public void testEnforceImageSizeImageTallerThanRequested() throws Exception {
		
		// createImageFromFilename
		BufferedImage mockedOriginalBufferedImage = Mockito.mock(BufferedImage.class);
		
		Mockito.when(mockedImageIOHandler.createImageFromFilename(Mockito.anyString())).thenReturn(mockedOriginalBufferedImage);
		
		Mockito.when(mockedOriginalBufferedImage.getWidth()).thenReturn(1);
		Mockito.when(mockedOriginalBufferedImage.getHeight()).thenReturn(4);
		
		// scaleImage
		BufferedImage mockedBufferedImageResponse = Mockito.mock(BufferedImage.class);
		Mockito.when(mockedImageIOHandler.scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble())).thenReturn(mockedBufferedImageResponse);
		Mockito.when(mockedBufferedImageResponse.getWidth()).thenReturn(2);
		Mockito.when(mockedBufferedImageResponse.getHeight()).thenReturn(3);
		
		// saveImage
		Mockito.doNothing().when(mockedImageIOHandler).saveImage(Mockito.any(BufferedImage.class), Mockito.anyString());
		
		Dimension result = imageScaler.enforceImageSize("/", 2.0);

		Mockito.verify(mockedImageIOHandler, Mockito.times(1)).createImageFromFilename("/");
		Mockito.verify(mockedOriginalBufferedImage, Mockito.times(1)).getWidth();
		Mockito.verify(mockedOriginalBufferedImage, Mockito.times(1)).getHeight();
		Mockito.verify(mockedImageIOHandler, Mockito.times(1)).scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble());
		Mockito.verify(mockedImageIOHandler, Mockito.times(1)).saveImage(mockedBufferedImageResponse, "/");
		Mockito.verify(mockedBufferedImageResponse, Mockito.times(1)).getWidth();
		Mockito.verify(mockedBufferedImageResponse, Mockito.times(1)).getHeight();
		
		assertEquals(2, result.getWidth(), 1.0);
		assertEquals(3, result.getHeight(), 1.0);
	}
	
	/**
	 * generateThumbnailsForFolder tests
	 */
	
	@Test(expected=NullPointerException.class)
	public void testGenerateThumbnailsForFolderUsingNulls() throws IOException {
		imageScaler.generateThumbnailsForFolder(null, null, null, -1.0);
	}
	
	@Test(expected=NullPointerException.class)
	public void testGenerateThumbnailsForFolderNullInputFolder() throws IOException {
		imageScaler.generateThumbnailsForFolder(null, "foo", "bar", 1.0);
	}

	@Test(expected=NullPointerException.class)
	public void testGenerateThumbnailsForFolderNullOutputFolder() throws IOException {
		imageScaler.generateThumbnailsForFolder("foo", null, "bar", 1.0);
	}

	@Test
	public void testGenerateThumbnailsForFolderNullPrefix() throws IOException {
		// this is ok
		imageScaler.generateThumbnailsForFolder("foo", "bar", null, 1.0);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testGenerateThumbnailsForFolderNegativeHeight() throws IOException {
		imageScaler.generateThumbnailsForFolder("foo", "bar", "baz", -1.0);
	}

	@Test
	public void testGenerateThumbnailsForEmptyFolder() throws IOException {
		
		List<String> mockFileList = new ArrayList<String>();
		Mockito.when(mockedFileIOHandler.listFilesInFolder("foo")).thenReturn(mockFileList);

		imageScaler.generateThumbnailsForFolder("foo", "bar", "prefix", 1.0);
		
		Mockito.verify(mockedFileIOHandler, Mockito.times(1)).listFilesInFolder("foo");
		Mockito.verify(mockedImageIOHandler, Mockito.times(0)).createImageFromFilename(Mockito.anyString());
		Mockito.verify(mockedImageIOHandler, Mockito.times(0)).scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble());
		Mockito.verify(mockedImageIOHandler, Mockito.times(0)).saveImage(Mockito.any(BufferedImage.class), Mockito.anyString());
		
	}

	@Test
	public void testGenerateThumbnailsForFolderWithInvalidFiles() throws IOException {
		
		List<String> mockFileList = new ArrayList<String>();
		mockFileList.add("file1");
		mockFileList.add("file2");
		mockFileList.add("file3");
		Mockito.when(mockedFileIOHandler.listFilesInFolder("foo")).thenReturn(mockFileList);

		imageScaler.generateThumbnailsForFolder("foo", "bar", "prefix", 1.0);
		
		Mockito.verify(mockedFileIOHandler, Mockito.times(1)).listFilesInFolder("foo");
		Mockito.verify(mockedImageIOHandler, Mockito.times(0)).createImageFromFilename(Mockito.anyString());
		Mockito.verify(mockedImageIOHandler, Mockito.times(0)).scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble());
		Mockito.verify(mockedImageIOHandler, Mockito.times(0)).saveImage(Mockito.any(BufferedImage.class), Mockito.anyString());
		
	}

	@Test
	public void testGenerateThumbnailsForFolderWithValieFiles() throws IOException {
		
		List<String> mockFileList = new ArrayList<String>();
		mockFileList.add("file1.png");
		mockFileList.add("file2.png");
		mockFileList.add("file3.png");
		Mockito.when(mockedFileIOHandler.listFilesInFolder("foo")).thenReturn(mockFileList);

		// createImageFromFilename
		BufferedImage mockedOriginalBufferedImage = Mockito.mock(BufferedImage.class);
		Mockito.when(mockedImageIOHandler.createImageFromFilename(Mockito.anyString())).thenReturn(mockedOriginalBufferedImage);
		Mockito.when(mockedOriginalBufferedImage.getHeight()).thenReturn(1);
		
		// scaleImage
		BufferedImage mockeScaledBufferedImage = Mockito.mock(BufferedImage.class);
		Mockito.when(mockedImageIOHandler.scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble())).thenReturn(mockeScaledBufferedImage);

		// saveImage
		Mockito.doNothing().when(mockedImageIOHandler).saveImage(Mockito.any(BufferedImage.class), Mockito.anyString());
		
		imageScaler.generateThumbnailsForFolder("foo", "bar", "prefix", 1.0);
		
		Mockito.verify(mockedFileIOHandler, Mockito.times(1)).listFilesInFolder("foo");
		Mockito.verify(mockedImageIOHandler, Mockito.times(3)).createImageFromFilename(Mockito.anyString());
		Mockito.verify(mockedImageIOHandler, Mockito.times(3)).scaleImage(Mockito.any(BufferedImage.class), Mockito.anyDouble());
		Mockito.verify(mockedImageIOHandler, Mockito.times(3)).saveImage(Mockito.any(BufferedImage.class), Mockito.anyString());
	
	}

	/**
	 * generateThumbnailForImage tests
	 */
	
	/**
	 * isAllowedFileFormat tests
	 */

}
