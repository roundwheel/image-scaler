package ca.roundwheel.scaler;

import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import org.junit.Test;

import ca.roundwheel.scaler.ImageScaler;
import junit.framework.TestCase;

/**
 * To successfully run <code>TestImageThumbnailer</code> tests, 
 * ensure that the <code>FOLDER_IMAGE</code> and <code>FOLDER_THUMB</code> 
 * folders exist, and that there is an image with the names specified by 
 * <code>TEST_IMAGE_LANDSCAPE</code> and <code>TEST_IMAGE_PORTRAIT</code>. 
 *
 * @author <a href="mailto:yvon@600-6604.com">Yvon Comeau</a>
 *
 * TODO - these are integration tests!
 */
public class TestImageScaler extends TestCase {


	@Test
	public void testEnforceImageSizeNullPath() throws IOException {
		assertTrue(true);
	}
	
//	private ImageScaler thumbnailer;
//	
//	private static String ROOT_FOLDER;
//	
//	static {
//		try {
//			ROOT_FOLDER = new File(".").getCanonicalPath();
//			System.out.println("Running tests in " + ROOT_FOLDER);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	private static final String FOLDER_IMAGE = ROOT_FOLDER + "/test/resources/images/";
//	private static final String FOLDER_THUMB = ROOT_FOLDER + "/test/resources/thumbs/";
//	private static final String TEST_IMAGE_LANDSCAPE = "nelly-landscape.jpg";
//	private static final String TEST_IMAGE_PORTRAIT = "tinariwan-portrait.jpg";
//	private static final String TEST_IMAGE_TOO_WIDE_AND_TALL = "ping-hangin.jpg";
//	private static final String TEST_IMAGE_ERROR = "pingtype.jpg";
//	
//	private static final String THUMBNAIL_PREFIX = "";
//	private static final int MAXIMUM_IMAGE_LENGTH = 450;
//	
//	@Override
//	public void setUp() {
//		thumbnailer = new ImageScaler();
//	}
//	
//	public void testNullImage() throws IOException {		
//		thumbnailer.generateThumbnailForImage(null, null, 65.0);
//		thumbnailer.generateThumbnailForImage(null, "test", 65.0);
//		thumbnailer.generateThumbnailForImage("test", null, 65.0);
//	}
//	
//	public void testGoodImage() throws IOException {
//		String fullImage = FOLDER_IMAGE + TEST_IMAGE_LANDSCAPE;
//		String thumbnailImage = FOLDER_THUMB + TEST_IMAGE_LANDSCAPE;
//		
//		//System.out.println("Testing " + fullImage);
//		
//		// Generate the thumbnail
//		thumbnailer.generateThumbnailForImage(fullImage, thumbnailImage, 65.0);
//		
//		// Check that the thumbnail was created
//		assertFileExists(thumbnailImage);
//	}
//	
//	public void testNullFolder() throws IOException {
//		thumbnailer.generateThumbnailsForFolder(null, null, null, 65.0);
//		thumbnailer.generateThumbnailsForFolder(null, "test", null, 65.0);
//		thumbnailer.generateThumbnailsForFolder("test", null, null, 65.0);
//	}
//	
//	public void testGoodFolder() throws IOException {
//		thumbnailer.generateThumbnailsForFolder(FOLDER_IMAGE, FOLDER_THUMB, THUMBNAIL_PREFIX, 65.0);
//		
//		File imageFolder = new File(FOLDER_IMAGE);
//		
//		for (String originalFilename: imageFolder.list()) {
//			// For each file in the image folder, check that the thumbnail was created
//			if (thumbnailer.isAllowedFileFormat(originalFilename)) {
//				String thumbFileName = FOLDER_THUMB + THUMBNAIL_PREFIX + originalFilename;
//				assertFileExists(thumbFileName);
//			}
//		}
//	}
//
//	public void testEnforceLandscapeImageSize() throws IOException {
//		resizeAndAssert(TEST_IMAGE_LANDSCAPE);
//	}
//
//	public void testEnforcePortraitImageSize() throws IOException {
//		resizeAndAssert(TEST_IMAGE_PORTRAIT);
//	}
//	
//	public void testEnforceWideAndTallImage() throws IOException {
//		resizeAndAssert(TEST_IMAGE_TOO_WIDE_AND_TALL);
//	}
//	
//	public void testEnforceProblemImage() throws IOException {
//		resizeAndAssert(TEST_IMAGE_ERROR);
//	}
//	
//	private void resizeAndAssert(String testImage) throws IOException {
//		String fullImage = FOLDER_IMAGE + testImage;
//		String backupImage = FOLDER_IMAGE + "backup-" + testImage;
//		
//		backupFile(fullImage, backupImage);
//	      
//		Dimension dimension = thumbnailer.enforceImageSize(backupImage, MAXIMUM_IMAGE_LENGTH);
//		assertTrue(dimension.getWidth() <= MAXIMUM_IMAGE_LENGTH);
//		assertTrue(dimension.getHeight() <= MAXIMUM_IMAGE_LENGTH);
//	}
//
//	/**
//	 * Check that the file was created.
//	 * 
//	 * @param thumbFileName		The name of the thumbnail file 
//	 */
//	private void assertFileExists(String thumbFileName) {
//		File thumbFile = new File(thumbFileName);
//		assertTrue(thumbFile.exists());
//	}
//
//	/**
//	 * A helper method to make a copy of an image file.
//	 * 
//	 * @param fullImage		The full path to the original file
//	 * @param backupImage	The full path where the backup should be created
//	 * 
//	 * @throws FileNotFoundException
//	 * @throws IOException
//	 */
//	private void backupFile(String fullImage, String backupImage)
//			throws FileNotFoundException, IOException {
//		FileInputStream fIn = new FileInputStream(fullImage);
//		FileOutputStream fOut = new FileOutputStream(backupImage);
//		FileChannel fIChan, fOChan;
//		
//		fIChan = fIn.getChannel();
//	    fOChan = fOut.getChannel();
//
//	    long fSize = fIChan.size();
//
//	    MappedByteBuffer mBuf = fIChan.map(FileChannel.MapMode.READ_ONLY, 0, fSize);
//
//	    // this copies the file
//	    fOChan.write(mBuf); 
//
//	    fIChan.close();
//	    fIn.close();
//
//	    fOChan.close();
//	    fOut.close();
//	}

}
