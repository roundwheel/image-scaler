package ca.roundwheel.scaler.fileio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests {@code FileIOHandler}.
 * 
 * @author Yvon Comeau
 */
public class FileIOHandlerTest {

	// The object we are testing
	private FileIOHandler fileIOHandler;

	@Before
	public void init() {
		fileIOHandler = new FileIOHandler();
	}
	
	
	/**
	 * listFilesInFolder tests
	 */
	
	@Test(expected=NullPointerException.class)
	public void testListFilesInFolderNullFolder() {
		fileIOHandler.listFilesInFolder(null);
	}
	
	@Test
	public void testListFilesInFolderInvalidFolder() {
		assertEquals(0, fileIOHandler.listFilesInFolder("/foo/bar").size());
	}
	
	@Test
	public void testListFilesInFolderValidFolder() {
		assertTrue(fileIOHandler.listFilesInFolder(".").size() > 0);
	}
}