package ca.roundwheel.scaler.imageio;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests {@code ImageIOHandler}.
 * 
 * @author Yvon Comeau
 */
public class ImageIOHandlerTest {

	// The object we are testing
	private ImageIOHandler imageIOHandler;

	/**
	 * createImageFromFilename
	 */
	@Test
	public void testCreateImageFromFilename() {
		assertTrue(true);
	}
	
	/**
	 * scaleImage
	 */
	
	/**
	 * saveImage
	 */
	
//	@Test
//	public void testEnforceImageSizeImageWiderThanRequested() throws Exception {
//
//		BufferedImage mockedOriginalBufferedImage = Mockito.mock(BufferedImage.class);
//		BufferedImage mockedScaledBufferedImage = Mockito.mock(BufferedImage.class);
//		ColorModel mockedColorModel = Mockito.mock(ColorModel.class);
//		
//		// createImageFromFilename
//		PowerMockito.mockStatic(ImageIO.class);
//		PowerMockito.when(ImageIO.read(Mockito.any(File.class))).thenReturn(mockedOriginalBufferedImage);
//		Mockito.when(mockedOriginalBufferedImage.getColorModel()).thenReturn(mockedColorModel);
//		Mockito.when(mockedOriginalBufferedImage.getWidth()).thenReturn(4);
//		Mockito.when(mockedOriginalBufferedImage.getHeight()).thenReturn(1);
//
//		// scaleImage
//		AffineTransform mockedAffineTransform = Mockito.mock(AffineTransform.class);
//		PowerMockito.mockStatic(AffineTransform.class);
//		PowerMockito.when(AffineTransform.getScaleInstance(Mockito.anyDouble(), Mockito.anyDouble())).thenReturn(mockedAffineTransform);
//		
//		// we use PowerMockito's mock because the filter method is final
//		AffineTransformOp mockedScaleOperation = PowerMockito.mock(AffineTransformOp.class);
//		PowerMockito.whenNew(AffineTransformOp.class).withArguments(Mockito.any(AffineTransform.class), Mockito.anyInt()).thenReturn(mockedScaleOperation);
//		Mockito.when(mockedScaleOperation.createCompatibleDestImage(Mockito.any(BufferedImage.class), Mockito.any(ColorModel.class))).thenReturn(mockedScaledBufferedImage);
//		Mockito.when(mockedScaleOperation.filter(mockedOriginalBufferedImage, mockedScaledBufferedImage)).thenReturn(mockedScaledBufferedImage);
//				
//		// saveImage
//		PowerMockito.when(ImageIO.write(Mockito.any(BufferedImage.class), Mockito.anyString(), Mockito.any(File.class))).thenReturn(true);
//		
//		Mockito.when(mockedScaledBufferedImage.getWidth()).thenReturn(3);
//		Mockito.when(mockedScaledBufferedImage.getHeight()).thenReturn(1);
//		
//		Dimension result = imageScaler.enforceImageSize("/", 2.0);
//
//		Mockito.verify(mockedScaleOperation, Mockito.times(1)).createCompatibleDestImage(mockedOriginalBufferedImage, mockedColorModel);
//		Mockito.verify(mockedScaleOperation, Mockito.times(1)).filter(mockedOriginalBufferedImage, mockedScaledBufferedImage);
//		Mockito.verify(mockedScaledBufferedImage, Mockito.times(1)).getWidth();
//		Mockito.verify(mockedScaledBufferedImage, Mockito.times(2)).getHeight();
//		
//		assertEquals(3, result.getWidth(), 1.0);
//		assertEquals(1, result.getHeight(), 1.0);
//	}
//	
//	@Test
//	public void testEnforceImageSizeImageTallerThanRequested() throws Exception {
//
//		BufferedImage mockedOriginalBufferedImage = Mockito.mock(BufferedImage.class);
//		BufferedImage mockedScaledBufferedImage = Mockito.mock(BufferedImage.class);
//		ColorModel mockedColorModel = Mockito.mock(ColorModel.class);
//		
//		// createImageFromFilename
//		PowerMockito.mockStatic(ImageIO.class);
//		PowerMockito.when(ImageIO.read(Mockito.any(File.class))).thenReturn(mockedOriginalBufferedImage);
//		Mockito.when(mockedOriginalBufferedImage.getColorModel()).thenReturn(mockedColorModel);
//		Mockito.when(mockedOriginalBufferedImage.getWidth()).thenReturn(1);
//		Mockito.when(mockedOriginalBufferedImage.getHeight()).thenReturn(4);
//
//		// scaleImage
//		AffineTransform mockedAffineTransform = Mockito.mock(AffineTransform.class);
//		PowerMockito.mockStatic(AffineTransform.class);
//		PowerMockito.when(AffineTransform.getScaleInstance(Mockito.anyDouble(), Mockito.anyDouble())).thenReturn(mockedAffineTransform);
//		
//		// we use PowerMockito's mock because the filter method is final
//		AffineTransformOp mockedScaleOperation = PowerMockito.mock(AffineTransformOp.class);
//		PowerMockito.whenNew(AffineTransformOp.class).withArguments(Mockito.any(AffineTransform.class), Mockito.anyInt()).thenReturn(mockedScaleOperation);
//		Mockito.when(mockedScaleOperation.createCompatibleDestImage(Mockito.any(BufferedImage.class), Mockito.any(ColorModel.class))).thenReturn(mockedScaledBufferedImage);
//		Mockito.when(mockedScaleOperation.filter(mockedOriginalBufferedImage, mockedScaledBufferedImage)).thenReturn(mockedScaledBufferedImage);
//				
//		// saveImage
//		PowerMockito.when(ImageIO.write(Mockito.any(BufferedImage.class), Mockito.anyString(), Mockito.any(File.class))).thenReturn(true);
//		
//		Mockito.when(mockedScaledBufferedImage.getWidth()).thenReturn(1);
//		Mockito.when(mockedScaledBufferedImage.getHeight()).thenReturn(3);
//		
//		Dimension result = imageScaler.enforceImageSize("/", 2.0);
//
//		Mockito.verify(mockedScaleOperation, Mockito.times(1)).createCompatibleDestImage(mockedOriginalBufferedImage, mockedColorModel);
//		Mockito.verify(mockedScaleOperation, Mockito.times(1)).filter(mockedOriginalBufferedImage, mockedScaledBufferedImage);
//		Mockito.verify(mockedScaledBufferedImage, Mockito.times(1)).getWidth();
//		Mockito.verify(mockedScaledBufferedImage, Mockito.times(1)).getHeight();
//		
//		assertEquals(1, result.getWidth(), 1.0);
//		assertEquals(3, result.getHeight(), 1.0);
//	}
}
