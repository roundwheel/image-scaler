package ca.roundwheel.scaler.fileio;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * {@code FileIOHandler} handles the low level file operations.
 *
 * @author Yvon Comeau
 */
public class FileIOHandler {

	/**
	 * Returns the names of the files found in a folder.
	 * 
	 * @param folder  the full path to the folder to scan
	 * @return  a list of filenames in the folder, or an empty list if the folder does not exist or if the folder is empty
	 */
	public List<String> listFilesInFolder(String folder) {
		
		if (folder == null) {
			throw new NullPointerException("'folder' parameter cannot be null");
		}
		
		File imageFolder = new File(folder);
		
		if (imageFolder.exists()) {
			return Arrays.asList(imageFolder.list());
		}
		
		return Collections.emptyList();
	}
}
