package ca.roundwheel.scaler.imageio;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * {@code ImageHandler} handles the low level image operations.
 *
 * @author Yvon Comeau
 */
public class ImageIOHandler {
	
	private static final String THUMBNAIL_FORMAT_PNG = "png";
	
	/**
	 * Given the full path to an image, create an Image object.
	 * 
	 * @param inputPath 		The full path to an image
	 * @return					The full sized Image object
	 * @throws IOException 
	 */
	public BufferedImage createImageFromFilename(String inputPath) throws IOException {
		// TODO - move this into a FileHandler object
		File imageFile = new File(inputPath);
		return ImageIO.read(imageFile);
	}
	
	/**
	 * Given the image input stream, create an Image object.
	 * 
	 * @param imageInputStream 	The image as an input stream
	 * @return					The full sized Image object
	 * @throws IOException 
	 */
	public BufferedImage createImageFromInputStream(InputStream imageInputStream) throws IOException {
		return ImageIO.read(imageInputStream);
	}

	/**
	 * Scales an image, given an original image and a scale factor.
	 * 
	 * @param originalImage    The original image to scale 
	 * @param scaleFactor      The factor by which to scale the image
	 * @return                 The scaled image
	 * @throws IOException
	 */
	public BufferedImage scaleImage(BufferedImage originalImage, double scaleFactor) throws IOException {
		
		RenderingHints hints = new RenderingHints(null);
		hints.put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		hints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		hints.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		hints.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		hints.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
								
		AffineTransform transformObj = AffineTransform.getScaleInstance(scaleFactor, scaleFactor);
		
		BufferedImageOp scaleOperation = new AffineTransformOp(transformObj, AffineTransformOp.TYPE_BICUBIC);

		BufferedImage scaledImage = scaleOperation.createCompatibleDestImage(originalImage, originalImage.getColorModel());
		
		try {
			scaleOperation.filter(originalImage, scaledImage);			
		} catch (ImagingOpException e) {
			// If the above fails, try the slower but safer scale operation
			double width = originalImage.getWidth() * scaleFactor;
			double height = originalImage.getHeight() * scaleFactor;
			Image safeScaledImage = originalImage.getScaledInstance((int)width, (int)height, Image.SCALE_SMOOTH);
			scaledImage = createBufferedImage(safeScaledImage);
		}
		
		return scaledImage;
	}

	/**
	 * Save the new image to disk in the specified folder.
	 * 
	 * @param image			The image to save
	 * @param destinationPath	The full path of the destination image
	 * @throws IOException
	 */
	public void saveImage(BufferedImage bufferedImage, String destinationPath) throws IOException {
		File thumbFile = new File(destinationPath);
		// TODO - handle a false scenario
		ImageIO.write(bufferedImage, THUMBNAIL_FORMAT_PNG, thumbFile);
	}
	
	/**
	 * Convert the image into a BufferedImage.
	 * 
	 * @param image			The Image object
	 * @see 				{@link http://forums.sun.com/thread.jspa?threadID=636203&messageID=3708052}
	 */
	private BufferedImage createBufferedImage(Image image) {
        if(image instanceof BufferedImage) {
            return (BufferedImage)image;
        }
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        BufferedImage bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphic = bufferedImage.createGraphics();
        graphic.drawImage(image, 0, 0, null);
        graphic.dispose();
        return bufferedImage;
	}
}