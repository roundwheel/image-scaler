/**
 * Created 17-Aug-08
 */
package ca.roundwheel.scaler;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import ca.roundwheel.scaler.fileio.FileIOHandler;
import ca.roundwheel.scaler.imageio.ImageIOHandler;

/**	
 * {@code ImageScaler} is used to generate thumbnail 
 * images from a full sized image for use with SimpleViewer.
 *
 * @author <a href="mailto:yvon@600-6604.com">Yvon Comeau</a> 
 * @see <a href="http://bit.ly/cBsT7v">Using the Java 2D AffineTransformOp Filter</a>
 */
public class ImageScaler {

	/**
	 * Handles the low level image operations.
	 */
	private ImageIOHandler imageIOHandler;

	/**
	 * Handles the low level file operations.
	 */
	private FileIOHandler fileIOHandler;

	// Supported file formats
	public static final String ALLOWED_IMAGE_EXTENSIONS = ".jpg, .jpeg, .gif, .png";

	private String inputPath;
	private String destinationPath;
	
	/**
	 * Creates a new instance of a {@code ImageScaler} object.
	 */
	public ImageScaler() {
		this.imageIOHandler = new ImageIOHandler();
	}
	
	/**
	 * Creates a new instance of a {@code ImageScaler} object using the specified 
	 * {@code ImageIOHandler} and {@code FileIOHandler} objects.  This constructor 
	 * can be used to facilitate unit testing.
	 * 
	 * @param imageHandler  handles file IO
	 * @param fileHandler  handles file/folder IO
	 */
	public ImageScaler(ImageIOHandler imageHandler, FileIOHandler fileHandler) {
		this.imageIOHandler = imageHandler;
		this.fileIOHandler = fileHandler;
	}
	
	/**
	 * Check to make sure that an image is not wider than the maximum 
	 * allowed width.
	 * 
	 * @param imagePath			The full path to the image to scale.
	 * @param maximumLength		The maximum allowed width/height of the image
	 * @return					The width/height of the resulting image
	 * 
	 * @throws IOException
	 */
	public Dimension enforceImageSize(String imagePath, double maximumLength) throws IOException {

		if (imagePath == null) {
			throw new NullPointerException("'imagePath' parameter cannot be null");
		}
		
		if (maximumLength <= 0.0) {
			throw new IllegalArgumentException("'maximumLength' should be a positive, non-zero number");
		}
		
		// Create the image
		BufferedImage originalImage = imageIOHandler.createImageFromFilename(imagePath);
		
		// Shrink it
		int fullWidth = originalImage.getWidth();
		int fullHeight = originalImage.getHeight();

		Dimension result = new Dimension(fullWidth, fullHeight);
		
		// Is the image too wide?
		if (fullWidth > maximumLength) {
			double scale = maximumLength / fullWidth ;
		    BufferedImage scaledImage = imageIOHandler.scaleImage(originalImage, scale);
			// Overwrite the existing image
		    imageIOHandler.saveImage(scaledImage, imagePath);
			result = new Dimension(scaledImage.getWidth(), scaledImage.getHeight());
			// Since the resulting image could still be too tall...
			fullHeight = scaledImage.getHeight();
			originalImage = scaledImage;
		} 

		// Is the image too tall?
		if (fullHeight > maximumLength) {
			// Scale to make portrait images a sensible size
			double scale = maximumLength / fullHeight;
		    BufferedImage scaledImage = imageIOHandler.scaleImage(originalImage, scale);
			// Overwrite the existing image
		    imageIOHandler.saveImage(scaledImage, imagePath);
			result = new Dimension(scaledImage.getWidth(), scaledImage.getHeight());			
		}
		 
		// Return the dimensions of the final image		
		return result;
	}
	
	/**
	 * Generate thumbnails for all files in a folder.  The resulting 
	 * files are written to the specified outout folder, prefixed 
	 * with the optional prefix.
	 * 
	 * @param inputFolder		The folder containing all images to thumbnail.
	 * @param outputFolder		The folder where all thumbnails will be written.
	 * @param prefix			The optional thumbnail prefix.
	 * @param height			The maximum height to use for scaling the image
	 * 
	 * @throws IOException 
	 */
	public void generateThumbnailsForFolder(String inputFolder, String outputFolder, String prefix, double height) throws IOException {
		
		if (inputFolder == null) {
			throw new NullPointerException("'inputFolder' parameter cannot be null");
		}
		
		if (outputFolder == null) {
			throw new NullPointerException("'outputFolder' parameter cannot be null");
		}
		
		if (height <= 0.0) {
			throw new IllegalArgumentException("'height' should be a positive, non-zero number");
		}
		
		List<String> imageFiles = fileIOHandler.listFilesInFolder(inputFolder);

		// For each file in the image folder, generate the thumbnail
		for (String originalFilename: imageFiles) {
			if (isAllowedFileFormat(originalFilename)) {
				String originalFileName = inputFolder + originalFilename;
				String thumbFileName = outputFolder + prefix + originalFilename;	
				generateThumbnailForImage(originalFileName, thumbFileName, height);
			}
		}

	}
	
	/**
	 * Generate a thumbnail image and save it to disk in the specified folder.
	 * 
	 * @param inPath			The full path to the image to scale.
	 * @param destPath			The full path where the thumbnail should be saved.
	 * @param height			The maximum height to use for scaling the image
	 * 
	 * @throws IOException 
	 */
	public void generateThumbnailForImage(String inPath, String destPath, double height) throws IOException {
		
		if (inPath == null || destPath == null) {
			return;	
		}
		
		this.inputPath = inPath;
		this.destinationPath = destPath;
		
		// Create the image
		BufferedImage originalImage = imageIOHandler.createImageFromFilename(this.inputPath);
		
		// Shrink it
		double scale = height / originalImage.getHeight() ;
		BufferedImage thumbnail = imageIOHandler.scaleImage(originalImage, scale);
		
		// Save the thumbnail
		imageIOHandler.saveImage(thumbnail, this.destinationPath);
	}
	
	/**
	 * Generate and return a thumbnail image from the given input stream.
	 * 
	 * @param imageInputStream	The image as an input stream
	 * @param height			The maximum height to use for scaling the image
	 * 
	 * @throws IOException 
	 */
	public BufferedImage generateThumbnailForImage(InputStream imageInputStream, double height) throws IOException {
		
		if (imageInputStream == null) {
			return null;	
		}
		
		// Create the image
		BufferedImage originalImage = imageIOHandler.createImageFromInputStream(imageInputStream);
		
		// Shrink it
		double scale = height / originalImage.getHeight() ;
		return imageIOHandler.scaleImage(originalImage, scale);
	}

	/**
	 * Check to see if this file is of an allowed image format.
	 * 
	 * @param filename		The name of the file
	 * @return				True if it's a valid image format, false otherwise.
	 */
	public boolean isAllowedFileFormat(String filename) {
		
		if (filename != null) {
			String[] tokens = filename.split("\\.(?=[^\\.]+$)");
			if (tokens.length == 2) {
				return ALLOWED_IMAGE_EXTENSIONS.contains(tokens[1].toLowerCase());
			}
		}

		return false;
	}
}
