# image-scaler
Simple utility used for scaling images, typically to make thumbnails.

## Usage

```java
ImageScaler imageScaler = new ImageScaler();
BufferedImage thumbnail = imageScaler.generateThumbnailForImage(inputStream, HEIGHT);         
```

## Test coverage

To generate unit test coverate reports (using JaCoCo), run the following Maven command:

```
mvn clean package
```

... and open the report in target/site/jacoco/index.html